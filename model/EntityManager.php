<?php

/**
 * Created by PhpStorm.
 * User: vittore
 * Date: 09/01/16
 * Time: 12:28
 */
class EntityManager
{
    protected $dbConnection;

    public function __construct()
    {
        include_once(__DIR__ . '/../config.php');
        $this->dbConnection = new mysqli('localhost', $dbUser, $dbPassword, $db);
    }

    public function save($tipo, $oggetto)
    {
        if ($tipo == 'Entry') {
            $nome = $oggetto->getName();
            $numero = $oggetto->getNumber();
            $sql = "INSERT INTO entry (name,number) VALUES ('$nome','$numero')";
            $this->dbConnection->query($sql);
        }
        if ($tipo == 'User') {
            $username = $oggetto->getUsername();
            $password = $oggetto->getPassword();
            $sql = "INSERT INTO user (username,password) VALUES ('$username','$password')";
            $this->dbConnection->query($sql);
        }

    }

    public function find($tipo, $id)
    {
        if ($tipo == 'Entry') {
            $sql = "SELECT * from entry WHERE id=" . $id;
            $result = $this->dbConnection->query($sql);
            $result = $result->fetch_assoc();
            $entry = new Entry();
            $entry->setId($result['id']);
            $entry->setNumber($result['number']);
            $entry->setName($result['name']);

            return $entry;
        }
        if ($tipo == 'User') {
            $sql = "SELECT * from user WHERE id=" . $id;
            $result = $this->dbConnection->query($sql);
            $result = $result->fetch_assoc();
            $user = new User();
            $user->setId($result['id']);
            $user->setUsername($result['number']);
            $user->setPassword($result['name']);

            return $user;
        }
    }

    public function findAll($tipo)
    {
        $list = array();
        if ($tipo == 'Entry') {
            $sql = "SELECT * from entry";
            $results = $this->dbConnection->query($sql);
            while ($result = $results->fetch_assoc()) {
                $entry = new Entry();
                $entry->setId($result['id']);
                $entry->setNumber($result['number']);
                $entry->setName($result['name']);
                $list[] = $entry;
            }
        }
        if ($tipo == 'User') {
            $sql = "SELECT * from user";
            $results = $this->dbConnection->query($sql);
            while ($result = $results->fetch_assoc()) {
                $user = new User();
                $user->setId($result['id']);
                $user->setUsername($result['number']);
                $user->setPassword($result['name']);
                $list[] = $user;
            }
        }
        return $list;
    }


    public function remove($tipo, $id)
    {
        if ($tipo == 'Entry') {
            $sql = "DELETE from entry WHERE id=" . $id;
            $result = $this->dbConnection->query($sql);
        }
        if ($tipo == 'User') {
            $sql = "DELETE from user WHERE id=" . $id;
            $result = $this->dbConnection->query($sql);
            }
    }

}